<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A66196">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation</title>
    <author>England and Wales. Sovereign (1694-1702 : William III)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A66196 of text R40699 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing W2442). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A66196</idno>
    <idno type="STC">Wing W2442</idno>
    <idno type="STC">ESTC R40699</idno>
    <idno type="EEBO-CITATION">19524664</idno>
    <idno type="OCLC">ocm 19524664</idno>
    <idno type="VID">108955</idno>
    <idno type="PROQUESTGOID">2240856871</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A66196)</note>
    <note>Transcribed from: (Early English Books Online ; image set 108955)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1679:12)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation</title>
      <author>England and Wales. Sovereign (1694-1702 : William III)</author>
      <author>William, III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Charles Bill, and the executrix of Thomas Newcomb ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1697.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in the British Library.</note>
      <note>"Given at our court at Kensington the seventeenth day of November, 1697, in the ninth year of our reign."</note>
      <note>Postpones the meeting of Parliament from Nov. 23 to Dec. 3, 1697.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Proclamations -- Great Britain.</term>
     <term>Great Britain -- History -- William and Mary, 1689-1702.</term>
     <term>Great Britain -- Politics and government -- 1689-1702.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King, a proclamation. William R. His Majesty having been detained beyond the seas by contrary winds longer than he intended, and being desirous</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1697</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>241</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-02</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-07</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A66196-t">
  <body xml:id="A66196-e0">
   <div type="royal_proclamation" xml:id="A66196-e10">
    <pb facs="tcp:108955:1" rend="simple:additions" xml:id="A66196-001-a"/>
    <head xml:id="A66196-e20">
     <figure xml:id="A66196-e30">
      <p xml:id="A66196-e40">
       <w lemma="w" pos="ab" xml:id="A66196-001-a-0010">W</w>
       <w lemma="r" pos="sy" xml:id="A66196-001-a-0020">R</w>
      </p>
      <p xml:id="A66196-e50">
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0060">DROIT</w>
      </p>
      <p xml:id="A66196-e60">
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0090">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A66196-001-a-0120">PENSE</w>
      </p>
      <figDesc xml:id="A66196-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A66196-e80">
     <w lemma="by" pos="acp" xml:id="A66196-001-a-0130">By</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-0140">the</w>
     <w lemma="king" pos="n1" xml:id="A66196-001-a-0150">King</w>
     <pc xml:id="A66196-001-a-0160">,</pc>
    </byline>
    <head xml:id="A66196-e90">
     <w lemma="a" pos="d" xml:id="A66196-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A66196-001-a-0180">PROCLAMATION</w>
     <pc unit="sentence" xml:id="A66196-001-a-0190">.</pc>
     <add xml:id="A66196-e100">
      <w lemma="for" pos="acp" xml:id="A66196-001-a-0200">For</w>
      <w lemma="prorogue" pos="vvg" xml:id="A66196-001-a-0210">proroguing</w>
      <w lemma="n/a" orig="Parliamᵗ" pos="fla" xml:id="A66196-001-a-0220">Parliamt</w>
      <w lemma="from" pos="acp" xml:id="A66196-001-a-0240">from</w>
      <w lemma=".23" pos="crd" xml:id="A66196-001-a-0250">.23</w>
      <pc unit="sentence" xml:id="A66196-001-a-0260">.</pc>
      <w lemma="nou." pos="ab" reg="Nou." xml:id="A66196-001-a-0270">Nov.</w>
      <w lemma="1697." pos="crd" xml:id="A66196-001-a-0280">1697.</w>
      <w lemma="to" pos="acp" xml:id="A66196-001-a-0290">to</w>
      <w lemma=".3" pos="crd" xml:id="A66196-001-a-0300">.3</w>
      <pc unit="sentence" xml:id="A66196-001-a-0310">.</pc>
      <w lemma="dec." pos="ab" xml:id="A66196-001-a-0320">Dec.</w>
      <w lemma="next" pos="ord" xml:id="A66196-001-a-0330">next</w>
      <pc unit="sentence" xml:id="A66196-001-a-0340">.</pc>
     </add>
    </head>
    <opener xml:id="A66196-e120">
     <dateline xml:id="A66196-e130">
      <date xml:id="A66196-e140">
       <add xml:id="A66196-e150">
        <w lemma="18." pos="crd" xml:id="A66196-001-a-0350">18.</w>
        <w lemma="nou." pos="ab" reg="Nou." xml:id="A66196-001-a-0370">Nov.</w>
        <w lemma="1697." pos="crd" xml:id="A66196-001-a-0380">1697.</w>
        <pc unit="sentence" xml:id="A66196-001-a-0390"/>
       </add>
      </date>
     </dateline>
     <lb xml:id="A66196-e160"/>
     <signed xml:id="A66196-e170">
      <w lemma="WILLIAM" pos="nn1" xml:id="A66196-001-a-0400">WILLIAM</w>
      <w lemma="r." pos="ab" xml:id="A66196-001-a-0410">R.</w>
      <pc unit="sentence" xml:id="A66196-001-a-0420"/>
     </signed>
    </opener>
    <p xml:id="A66196-e180">
     <w lemma="his" pos="po" rend="decorinit" xml:id="A66196-001-a-0430">HIs</w>
     <w lemma="majesty" pos="n1" xml:id="A66196-001-a-0440">Majesty</w>
     <w lemma="have" pos="vvg" xml:id="A66196-001-a-0450">having</w>
     <w lemma="be" pos="vvn" reg="been" xml:id="A66196-001-a-0460">beén</w>
     <w lemma="detain" pos="vvn" xml:id="A66196-001-a-0470">detained</w>
     <w lemma="beyond" pos="acp" xml:id="A66196-001-a-0480">beyond</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-0490">the</w>
     <w lemma="sea" pos="n2" xml:id="A66196-001-a-0500">Seas</w>
     <w lemma="by" pos="acp" xml:id="A66196-001-a-0510">by</w>
     <w lemma="contrary" pos="j" xml:id="A66196-001-a-0520">contrary</w>
     <w lemma="wind" pos="n2" xml:id="A66196-001-a-0530">Winds</w>
     <w lemma="long" pos="avc-j" xml:id="A66196-001-a-0540">longer</w>
     <w lemma="than" pos="cs" xml:id="A66196-001-a-0550">than</w>
     <w lemma="he" pos="pns" xml:id="A66196-001-a-0560">He</w>
     <w lemma="intend" pos="vvd" xml:id="A66196-001-a-0570">intended</w>
     <pc xml:id="A66196-001-a-0580">,</pc>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-0590">and</w>
     <w lemma="be" pos="vvg" xml:id="A66196-001-a-0600">being</w>
     <w lemma="desirous" pos="j" xml:id="A66196-001-a-0610">desirous</w>
     <w lemma="that" pos="cs" xml:id="A66196-001-a-0620">that</w>
     <w lemma="there" pos="av" xml:id="A66196-001-a-0630">there</w>
     <w lemma="shall" pos="vmd" xml:id="A66196-001-a-0640">should</w>
     <w lemma="be" pos="vvi" xml:id="A66196-001-a-0650">be</w>
     <w lemma="a" pos="d" xml:id="A66196-001-a-0660">a</w>
     <w lemma="full" pos="j" xml:id="A66196-001-a-0670">full</w>
     <w lemma="appearance" pos="n1" xml:id="A66196-001-a-0680">Appearance</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-0690">of</w>
     <w lemma="member" pos="n2" xml:id="A66196-001-a-0700">Members</w>
     <w lemma="at" pos="acp" xml:id="A66196-001-a-0710">at</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-0720">the</w>
     <w lemma="open" pos="vvg" xml:id="A66196-001-a-0730">Opening</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-0740">of</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-0750">the</w>
     <w lemma="next" pos="ord" xml:id="A66196-001-a-0760">next</w>
     <w lemma="session" pos="n2" xml:id="A66196-001-a-0770">Sessions</w>
     <pc xml:id="A66196-001-a-0780">,</pc>
     <w lemma="his" pos="po" xml:id="A66196-001-a-0790">His</w>
     <w lemma="majesty" pos="n1" xml:id="A66196-001-a-0800">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A66196-001-a-0810">hath</w>
     <w lemma="think" pos="vvn" xml:id="A66196-001-a-0820">thought</w>
     <w lemma="fit" pos="j" xml:id="A66196-001-a-0830">fit</w>
     <pc xml:id="A66196-001-a-0840">,</pc>
     <w lemma="with" pos="acp" xml:id="A66196-001-a-0850">with</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-0860">the</w>
     <w lemma="advice" pos="n1" xml:id="A66196-001-a-0870">Advice</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-0880">of</w>
     <w lemma="his" pos="po" xml:id="A66196-001-a-0890">His</w>
     <w lemma="privy" pos="j" xml:id="A66196-001-a-0900">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66196-001-a-0910">Council</w>
     <pc xml:id="A66196-001-a-0920">,</pc>
     <w lemma="to" pos="prt" xml:id="A66196-001-a-0930">to</w>
     <w lemma="declare" pos="vvi" xml:id="A66196-001-a-0940">Declare</w>
     <w lemma="his" pos="po" xml:id="A66196-001-a-0950">His</w>
     <w lemma="pleasure" pos="n1" xml:id="A66196-001-a-0960">Pleasure</w>
     <pc xml:id="A66196-001-a-0970">,</pc>
     <w lemma="that" pos="cs" xml:id="A66196-001-a-0980">That</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-0990">the</w>
     <w lemma="parliament" pos="n1" xml:id="A66196-001-a-1000">Parliament</w>
     <w lemma="which" pos="crq" xml:id="A66196-001-a-1010">which</w>
     <w lemma="stand" pos="vvz" xml:id="A66196-001-a-1020">stands</w>
     <w lemma="now" pos="av" xml:id="A66196-001-a-1030">now</w>
     <w lemma="prorogue" pos="vvn" xml:id="A66196-001-a-1040">Prorogued</w>
     <w lemma="to" pos="acp" xml:id="A66196-001-a-1050">to</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-1060">the</w>
     <w lemma="three" pos="crd" xml:id="A66196-001-a-1070">Three</w>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1080">and</w>
     <w lemma="twenty" pos="ord" xml:id="A66196-001-a-1090">twentieth</w>
     <w lemma="day" pos="n1" xml:id="A66196-001-a-1100">Day</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-1110">of</w>
     <w lemma="this" pos="d" xml:id="A66196-001-a-1120">this</w>
     <w lemma="instant" pos="j" xml:id="A66196-001-a-1130">Instant</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="A66196-001-a-1140">November</w>
     <pc xml:id="A66196-001-a-1150">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A66196-001-a-1160">shall</w>
     <w lemma="be" pos="vvi" xml:id="A66196-001-a-1170">be</w>
     <w lemma="then" pos="av" xml:id="A66196-001-a-1180">then</w>
     <w lemma="further" pos="avc-j" xml:id="A66196-001-a-1190">further</w>
     <w lemma="prorogue" pos="vvn" xml:id="A66196-001-a-1200">Prorogued</w>
     <w lemma="to" pos="acp" xml:id="A66196-001-a-1210">to</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-1220">the</w>
     <w lemma="three" pos="ord" xml:id="A66196-001-a-1230">Third</w>
     <w lemma="day" pos="n1" xml:id="A66196-001-a-1240">Day</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-1250">of</w>
     <w lemma="December" pos="nn1" rend="hi" xml:id="A66196-001-a-1260">December</w>
     <w lemma="next" pos="ord" xml:id="A66196-001-a-1270">next</w>
     <pc xml:id="A66196-001-a-1280">,</pc>
     <w lemma="on" pos="acp" xml:id="A66196-001-a-1290">on</w>
     <w lemma="which" pos="crq" xml:id="A66196-001-a-1300">which</w>
     <w lemma="day" pos="n1" xml:id="A66196-001-a-1310">Day</w>
     <w lemma="they" pos="pns" xml:id="A66196-001-a-1320">they</w>
     <w lemma="be" pos="vvb" xml:id="A66196-001-a-1330">are</w>
     <w lemma="to" pos="prt" xml:id="A66196-001-a-1340">to</w>
     <w lemma="meét" pos="vvi" xml:id="A66196-001-a-1350">Meét</w>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1360">and</w>
     <w lemma="sit" pos="vvb" reg="Sat" xml:id="A66196-001-a-1370">Sit</w>
     <w lemma="for" pos="acp" xml:id="A66196-001-a-1380">for</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-1390">the</w>
     <w lemma="dispatch" pos="vvb" xml:id="A66196-001-a-1400">Dispatch</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-1410">of</w>
     <w lemma="divers" pos="j" xml:id="A66196-001-a-1420">divers</w>
     <w lemma="weighty" pos="j" xml:id="A66196-001-a-1430">Weighty</w>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1440">and</w>
     <w lemma="important" pos="j" xml:id="A66196-001-a-1450">Important</w>
     <w lemma="affair" pos="n2" xml:id="A66196-001-a-1460">Affairs</w>
     <pc unit="sentence" xml:id="A66196-001-a-1470">.</pc>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1480">And</w>
     <w lemma="all" pos="d" xml:id="A66196-001-a-1490">all</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-1500">the</w>
     <w lemma="lord" pos="n2" xml:id="A66196-001-a-1510">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A66196-001-a-1520">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1530">and</w>
     <w lemma="temporal" pos="j" xml:id="A66196-001-a-1540">Temporal</w>
     <pc xml:id="A66196-001-a-1550">,</pc>
     <w lemma="knight" pos="n2" xml:id="A66196-001-a-1560">Knights</w>
     <pc xml:id="A66196-001-a-1570">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A66196-001-a-1580">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1590">and</w>
     <w lemma="burgess" pos="n2" xml:id="A66196-001-a-1600">Burgesses</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-1610">of</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-1620">the</w>
     <w lemma="house" pos="n1" xml:id="A66196-001-a-1630">House</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-1640">of</w>
     <w lemma="commons" pos="n2" xml:id="A66196-001-a-1650">Commons</w>
     <pc xml:id="A66196-001-a-1660">,</pc>
     <w lemma="be" pos="vvb" xml:id="A66196-001-a-1670">are</w>
     <w lemma="hereby" pos="av" xml:id="A66196-001-a-1680">hereby</w>
     <w lemma="require" pos="vvn" xml:id="A66196-001-a-1690">Required</w>
     <w lemma="and" pos="cc" xml:id="A66196-001-a-1700">and</w>
     <w lemma="command" pos="vvn" xml:id="A66196-001-a-1710">Commanded</w>
     <w lemma="to" pos="prt" xml:id="A66196-001-a-1720">to</w>
     <w lemma="give" pos="vvi" xml:id="A66196-001-a-1730">give</w>
     <w lemma="their" pos="po" xml:id="A66196-001-a-1740">their</w>
     <w lemma="attendance" pos="n1" xml:id="A66196-001-a-1750">Attendance</w>
     <w lemma="according" pos="av-j" xml:id="A66196-001-a-1760">accordingly</w>
     <w lemma="at" pos="acp" xml:id="A66196-001-a-1770">at</w>
     <w lemma="Westminster" pos="nn1" rend="hi" xml:id="A66196-001-a-1780">Westminster</w>
     <w lemma="on" pos="acp" xml:id="A66196-001-a-1790">on</w>
     <w lemma="the" pos="d" xml:id="A66196-001-a-1800">the</w>
     <w lemma="say" pos="vvd" xml:id="A66196-001-a-1810">said</w>
     <w lemma="three" pos="ord" xml:id="A66196-001-a-1820">Third</w>
     <w lemma="day" pos="n1" xml:id="A66196-001-a-1830">Day</w>
     <w lemma="of" pos="acp" xml:id="A66196-001-a-1840">of</w>
     <w lemma="December" pos="nn1" rend="hi" xml:id="A66196-001-a-1850">December</w>
     <w lemma="next" pos="ord" xml:id="A66196-001-a-1860">next</w>
     <pc unit="sentence" xml:id="A66196-001-a-1870">.</pc>
    </p>
    <closer xml:id="A66196-e230">
     <dateline xml:id="A66196-e240">
      <w lemma="give" pos="vvn" xml:id="A66196-001-a-1880">Given</w>
      <w lemma="at" pos="acp" xml:id="A66196-001-a-1890">at</w>
      <w lemma="our" pos="po" xml:id="A66196-001-a-1900">Our</w>
      <w lemma="court" pos="n1" xml:id="A66196-001-a-1910">Court</w>
      <w lemma="at" pos="acp" xml:id="A66196-001-a-1920">at</w>
      <w lemma="Kensington" pos="nn1" rend="hi" xml:id="A66196-001-a-1930">Kensington</w>
      <date xml:id="A66196-e260">
       <w lemma="the" pos="d" xml:id="A66196-001-a-1940">the</w>
       <w lemma="seventeen" pos="ord" xml:id="A66196-001-a-1950">Seventeenth</w>
       <w lemma="day" pos="n1" xml:id="A66196-001-a-1960">Day</w>
       <w lemma="of" pos="acp" xml:id="A66196-001-a-1970">of</w>
       <w lemma="November" pos="nn1" rend="hi" xml:id="A66196-001-a-1980">November</w>
       <pc xml:id="A66196-001-a-1990">,</pc>
       <w lemma="1697." pos="crd" xml:id="A66196-001-a-2000">1697.</w>
       <pc unit="sentence" xml:id="A66196-001-a-2010"/>
       <w lemma="in" pos="acp" xml:id="A66196-001-a-2020">In</w>
       <w lemma="the" pos="d" xml:id="A66196-001-a-2030">the</w>
       <w lemma="nine" pos="ord" xml:id="A66196-001-a-2040">Ninth</w>
       <w lemma="year" pos="n1" xml:id="A66196-001-a-2050">Year</w>
       <w lemma="of" pos="acp" xml:id="A66196-001-a-2060">of</w>
       <w lemma="our" pos="po" xml:id="A66196-001-a-2070">Our</w>
       <w lemma="reign" pos="n1" xml:id="A66196-001-a-2080">Reign</w>
       <pc unit="sentence" xml:id="A66196-001-a-2090">.</pc>
      </date>
     </dateline>
     <lb xml:id="A66196-e280"/>
     <hi xml:id="A66196-e290">
      <w lemma="God" pos="nn1" xml:id="A66196-001-a-2100">God</w>
      <w lemma="save" pos="vvb" xml:id="A66196-001-a-2110">save</w>
      <w lemma="the" pos="d" xml:id="A66196-001-a-2120">the</w>
      <w lemma="King" pos="n1" xml:id="A66196-001-a-2130">King</w>
      <pc unit="sentence" xml:id="A66196-001-a-2131">.</pc>
     </hi>
     <pc rend="follows-hi" unit="sentence" xml:id="A66196-001-a-2140"/>
    </closer>
   </div>
  </body>
  <back xml:id="A66196-e300">
   <div type="colophon" xml:id="A66196-e310">
    <p xml:id="A66196-e320">
     <hi xml:id="A66196-e330">
      <w lemma="LONDON" pos="nn1" rend="hi" xml:id="A66196-001-a-2150">LONDON</w>
      <pc xml:id="A66196-001-a-2160">,</pc>
      <w lemma="print" pos="vvn" xml:id="A66196-001-a-2170">Printed</w>
      <w lemma="by" pos="acp" xml:id="A66196-001-a-2180">by</w>
      <hi xml:id="A66196-e350">
       <w lemma="Charles" pos="nn1" xml:id="A66196-001-a-2190">Charles</w>
       <w lemma="bill" pos="n1" xml:id="A66196-001-a-2200">Bill</w>
      </hi>
      <pc rend="follows-hi" xml:id="A66196-001-a-2210">,</pc>
      <w lemma="and" pos="cc" xml:id="A66196-001-a-2220">and</w>
      <w lemma="the" pos="d" xml:id="A66196-001-a-2230">the</w>
      <w lemma="executrix" pos="n1" xml:id="A66196-001-a-2240">Executrix</w>
      <w lemma="of" pos="acp" xml:id="A66196-001-a-2250">of</w>
      <hi xml:id="A66196-e360">
       <w lemma="Thomas" pos="nn1" xml:id="A66196-001-a-2260">Thomas</w>
       <w lemma="newcomb" pos="nn1" xml:id="A66196-001-a-2270">Newcomb</w>
      </hi>
      <pc rend="follows-hi" xml:id="A66196-001-a-2280">,</pc>
      <w lemma="decease" pos="vvn" reg="deceased" xml:id="A66196-001-a-2290">deceas'd</w>
      <pc unit="sentence" xml:id="A66196-001-a-2300">.</pc>
      <w lemma="printer" pos="n2" xml:id="A66196-001-a-2310">Printers</w>
      <w lemma="to" pos="acp" xml:id="A66196-001-a-2320">to</w>
      <w lemma="the" pos="d" xml:id="A66196-001-a-2330">the</w>
      <w lemma="king" pos="n2" xml:id="A66196-001-a-2340">Kings</w>
      <w lemma="most" pos="avs-d" xml:id="A66196-001-a-2350">most</w>
      <w lemma="excellent" pos="j" xml:id="A66196-001-a-2360">Excellent</w>
      <w lemma="majesty" pos="n1" xml:id="A66196-001-a-2370">Majesty</w>
      <pc unit="sentence" xml:id="A66196-001-a-2380">.</pc>
      <w lemma="1697." pos="crd" xml:id="A66196-001-a-2390">1697.</w>
      <pc unit="sentence" xml:id="A66196-001-a-2400"/>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
