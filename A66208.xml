<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A66208">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King, a proclamation for apprehending and securing the person of Roderick Mackenzie</title>
    <author>England and Wales. Sovereign (1694-1702 : William III)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A66208 of text R37228 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing W2460). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A66208</idno>
    <idno type="STC">Wing W2460</idno>
    <idno type="STC">ESTC R37228</idno>
    <idno type="EEBO-CITATION">16272721</idno>
    <idno type="OCLC">ocm 16272721</idno>
    <idno type="VID">105224</idno>
    <idno type="PROQUESTGOID">2248513237</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A66208)</note>
    <note>Transcribed from: (Early English Books Online ; image set 105224)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1602:26)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King, a proclamation for apprehending and securing the person of Roderick Mackenzie</title>
      <author>England and Wales. Sovereign (1694-1702 : William III)</author>
      <author>William III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Charles Bill and the executrix of Thomas Newcomb, deceas'd ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1695/6.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Kensington the thirteenth day of February, 1695/6, in the eighth year of our reign."</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- William and Mary, 1689-1702.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King, a proclamation for apprehending and securing the person of Roderick Mackenzie.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1696</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>499</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-02</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-03</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-04</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-04</date>
    <label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A66208-t">
  <body xml:id="A66208-e0">
   <div type="proclamation" xml:id="A66208-e10">
    <pb facs="tcp:105224:1" xml:id="A66208-001-a"/>
    <head xml:id="A66208-e20">
     <figure xml:id="A66208-e30">
      <p xml:id="A66208-e40">
       <w lemma="WR" pos="sy" xml:id="A66208-001-a-0010">WR</w>
      </p>
      <p xml:id="A66208-e50">
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0020">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0030">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0040">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0050">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0060">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0070">PENSE</w>
      </p>
      <p xml:id="A66208-e60">
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0080">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0090">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0100">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0110">DROIT</w>
      </p>
     </figure>
     <lb xml:id="A66208-e70"/>
     <w lemma="by" pos="acp" xml:id="A66208-001-a-0120">By</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-0130">the</w>
     <w lemma="king" pos="n1" xml:id="A66208-001-a-0140">King</w>
     <pc xml:id="A66208-001-a-0150">,</pc>
     <w lemma="a" pos="d" xml:id="A66208-001-a-0160">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A66208-001-a-0170">PROCLAMATION</w>
     <w lemma="for" pos="acp" xml:id="A66208-001-a-0180">For</w>
     <w lemma="apprehend" pos="vvg" xml:id="A66208-001-a-0190">Apprehending</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-0200">and</w>
     <w lemma="secure" pos="vvg" xml:id="A66208-001-a-0210">Securing</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-0220">the</w>
     <w lemma="person" pos="n1" xml:id="A66208-001-a-0230">Person</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-0240">of</w>
     <hi xml:id="A66208-e80">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-0250">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-0260">Mackenzie</w>
      <pc unit="sentence" xml:id="A66208-001-a-0270">.</pc>
     </hi>
    </head>
    <opener xml:id="A66208-e90">
     <signed xml:id="A66208-e100">
      <w lemma="WILLIAM" pos="nn1" xml:id="A66208-001-a-0280">WILLIAM</w>
      <w lemma="r." pos="ab" xml:id="A66208-001-a-0290">R.</w>
      <pc unit="sentence" xml:id="A66208-001-a-0300"/>
     </signed>
    </opener>
    <p xml:id="A66208-e110">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A66208-001-a-0310">WHereas</w>
     <w lemma="we" pos="pns" xml:id="A66208-001-a-0320">We</w>
     <w lemma="have" pos="vvb" xml:id="A66208-001-a-0330">have</w>
     <w lemma="be" pos="vvn" xml:id="A66208-001-a-0340">been</w>
     <w lemma="inform" pos="vvn" xml:id="A66208-001-a-0350">Informed</w>
     <pc xml:id="A66208-001-a-0360">,</pc>
     <w lemma="that" pos="cs" xml:id="A66208-001-a-0370">That</w>
     <hi xml:id="A66208-e120">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-0380">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-0390">Mackenzie</w>
     </hi>
     <w lemma="have" pos="vvz" xml:id="A66208-001-a-0400">hath</w>
     <w lemma="be" pos="vvn" xml:id="A66208-001-a-0410">been</w>
     <w lemma="examine" pos="vvn" xml:id="A66208-001-a-0420">Examined</w>
     <pc xml:id="A66208-001-a-0430">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-0440">and</w>
     <w lemma="give" pos="vvn" xml:id="A66208-001-a-0450">given</w>
     <w lemma="evidence" pos="n1" xml:id="A66208-001-a-0460">Evidence</w>
     <w lemma="before" pos="acp" xml:id="A66208-001-a-0470">before</w>
     <w lemma="a" pos="d" xml:id="A66208-001-a-0480">a</w>
     <w lemma="committee" pos="n1" xml:id="A66208-001-a-0490">Committee</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-0500">of</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-0510">the</w>
     <w lemma="commons" pos="n2" xml:id="A66208-001-a-0520">Commons</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-0530">in</w>
     <w lemma="parliament" pos="n1" xml:id="A66208-001-a-0540">Parliament</w>
     <w lemma="assemble" pos="vvn" xml:id="A66208-001-a-0550">Assembled</w>
     <pc xml:id="A66208-001-a-0560">,</pc>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-0570">in</w>
     <w lemma="relation" pos="n1" xml:id="A66208-001-a-0580">Relation</w>
     <w lemma="to" pos="acp" xml:id="A66208-001-a-0590">to</w>
     <w lemma="several" pos="j" xml:id="A66208-001-a-0600">several</w>
     <w lemma="person" pos="n2" xml:id="A66208-001-a-0610">Persons</w>
     <pc xml:id="A66208-001-a-0620">,</pc>
     <w lemma="who" pos="crq" xml:id="A66208-001-a-0630">who</w>
     <w lemma="under" pos="acp" xml:id="A66208-001-a-0640">under</w>
     <w lemma="colour" pos="n1" xml:id="A66208-001-a-0650">Colour</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-0660">of</w>
     <w lemma="a" pos="d" xml:id="A66208-001-a-0670">an</w>
     <w lemma="act" pos="n1" xml:id="A66208-001-a-0680">Act</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-0690">of</w>
     <w lemma="parliament" pos="n1" xml:id="A66208-001-a-0700">Parliament</w>
     <w lemma="pass" pos="vvn" xml:id="A66208-001-a-0710">Passed</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-0720">in</w>
     <hi xml:id="A66208-e130">
      <w lemma="Scotland" pos="nn1" xml:id="A66208-001-a-0730">Scotland</w>
      <pc xml:id="A66208-001-a-0740">,</pc>
     </hi>
     <w lemma="for" pos="acp" xml:id="A66208-001-a-0750">for</w>
     <w lemma="settle" pos="vvg" xml:id="A66208-001-a-0760">Settling</w>
     <w lemma="a" pos="d" xml:id="A66208-001-a-0770">a</w>
     <w lemma="trade" pos="n1" xml:id="A66208-001-a-0780">Trade</w>
     <w lemma="to" pos="acp" xml:id="A66208-001-a-0790">to</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-0800">the</w>
     <hi xml:id="A66208-e140">
      <w lemma="east" pos="n1" xml:id="A66208-001-a-0810">East</w>
      <w lemma="indies" pos="nn2" xml:id="A66208-001-a-0820">Indies</w>
      <pc xml:id="A66208-001-a-0830">,</pc>
     </hi>
     <w lemma="have" pos="vvd" xml:id="A66208-001-a-0840">had</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-0850">in</w>
     <w lemma="this" pos="d" xml:id="A66208-001-a-0860">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A66208-001-a-0870">Kingdom</w>
     <w lemma="administer" pos="vvn" reg="Administered" xml:id="A66208-001-a-0880">Administred</w>
     <pc xml:id="A66208-001-a-0890">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-0900">and</w>
     <w lemma="take" pos="vvn" xml:id="A66208-001-a-0910">Taken</w>
     <w lemma="a" pos="d" xml:id="A66208-001-a-0920">an</w>
     <w lemma="oath" pos="n1" xml:id="A66208-001-a-0930">Oath</w>
     <hi xml:id="A66208-e150">
      <w lemma="n/a" pos="ffr" xml:id="A66208-001-a-0940">De</w>
      <w lemma="n/a" pos="fla" xml:id="A66208-001-a-0950">Fideli</w>
      <pc xml:id="A66208-001-a-0960">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-0970">and</w>
     <w lemma="have" pos="vvd" xml:id="A66208-001-a-0980">had</w>
     <w lemma="also" pos="av" xml:id="A66208-001-a-0990">also</w>
     <pc xml:id="A66208-001-a-1000">,</pc>
     <w lemma="under" pos="acp" xml:id="A66208-001-a-1010">under</w>
     <w lemma="colour" pos="n1" xml:id="A66208-001-a-1020">Colour</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-1030">of</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1040">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66208-001-a-1050">said</w>
     <w lemma="act" pos="n1" xml:id="A66208-001-a-1060">Act</w>
     <pc xml:id="A66208-001-a-1070">,</pc>
     <w lemma="style" pos="vvn" reg="Styled" xml:id="A66208-001-a-1080">Stiled</w>
     <w lemma="themselves" pos="pr" xml:id="A66208-001-a-1090">themselves</w>
     <w lemma="a" pos="d" xml:id="A66208-001-a-1100">a</w>
     <w lemma="company" pos="n1" xml:id="A66208-001-a-1110">Company</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1120">and</w>
     <w lemma="act" pos="vvn" xml:id="A66208-001-a-1130">Acted</w>
     <w lemma="as" pos="acp" xml:id="A66208-001-a-1140">as</w>
     <w lemma="such" pos="d" xml:id="A66208-001-a-1150">such</w>
     <pc xml:id="A66208-001-a-1160">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1170">and</w>
     <w lemma="raise" pos="j-vn" xml:id="A66208-001-a-1180">Raised</w>
     <w lemma="money" pos="n1" xml:id="A66208-001-a-1190">Money</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-1200">in</w>
     <w lemma="this" pos="d" xml:id="A66208-001-a-1210">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A66208-001-a-1220">Kingdom</w>
     <w lemma="for" pos="acp" xml:id="A66208-001-a-1230">for</w>
     <w lemma="carry" pos="vvg" xml:id="A66208-001-a-1240">Carrying</w>
     <w lemma="on" pos="acp" xml:id="A66208-001-a-1250">on</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1260">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66208-001-a-1270">said</w>
     <w lemma="company" pos="n1" xml:id="A66208-001-a-1280">Company</w>
     <pc xml:id="A66208-001-a-1290">;</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1300">And</w>
     <w lemma="that" pos="cs" xml:id="A66208-001-a-1310">that</w>
     <w lemma="since" pos="acp" xml:id="A66208-001-a-1320">since</w>
     <w lemma="his" pos="po" xml:id="A66208-001-a-1330">his</w>
     <w lemma="say" pos="j-vn" xml:id="A66208-001-a-1340">said</w>
     <w lemma="examination" pos="n1" xml:id="A66208-001-a-1350">Examination</w>
     <pc xml:id="A66208-001-a-1360">,</pc>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1370">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66208-001-a-1380">said</w>
     <hi xml:id="A66208-e160">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-1390">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-1400">Mackenzie</w>
     </hi>
     <w lemma="have" pos="vvg" xml:id="A66208-001-a-1410">having</w>
     <w lemma="be" pos="vvn" xml:id="A66208-001-a-1420">been</w>
     <w lemma="summon" pos="vvn" xml:id="A66208-001-a-1430">Summoned</w>
     <pc xml:id="A66208-001-a-1440">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1450">and</w>
     <w lemma="appear" pos="vvg" xml:id="A66208-001-a-1460">Appearing</w>
     <w lemma="before" pos="acp" xml:id="A66208-001-a-1470">before</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1480">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66208-001-a-1490">said</w>
     <w lemma="committee" pos="n1" xml:id="A66208-001-a-1500">Committee</w>
     <pc xml:id="A66208-001-a-1510">,</pc>
     <w lemma="have" pos="vvd" xml:id="A66208-001-a-1520">had</w>
     <w lemma="endeavour" pos="vvn" xml:id="A66208-001-a-1530">endeavoured</w>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-1540">to</w>
     <w lemma="suppress" pos="vvi" xml:id="A66208-001-a-1550">Suppress</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1560">the</w>
     <w lemma="evidence" pos="n1" xml:id="A66208-001-a-1570">Evidence</w>
     <w lemma="he" pos="pns" xml:id="A66208-001-a-1580">he</w>
     <w lemma="have" pos="vvd" xml:id="A66208-001-a-1590">had</w>
     <w lemma="before" pos="acp" xml:id="A66208-001-a-1600">before</w>
     <w lemma="give" pos="vvn" xml:id="A66208-001-a-1610">given</w>
     <w lemma="against" pos="acp" xml:id="A66208-001-a-1620">against</w>
     <w lemma="such" pos="d" xml:id="A66208-001-a-1630">such</w>
     <w lemma="person" pos="n2" xml:id="A66208-001-a-1640">Persons</w>
     <pc xml:id="A66208-001-a-1650">;</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1660">And</w>
     <w lemma="that" pos="cs" xml:id="A66208-001-a-1670">that</w>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-1680">to</w>
     <w lemma="avoid" pos="vvb" xml:id="A66208-001-a-1690">Avoid</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1700">the</w>
     <w lemma="manifestation" pos="n1" xml:id="A66208-001-a-1710">Manifestation</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-1720">of</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1730">the</w>
     <w lemma="truth" pos="n1" xml:id="A66208-001-a-1740">Truth</w>
     <pc xml:id="A66208-001-a-1750">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1760">and</w>
     <pc join="right" xml:id="A66208-001-a-1770">(</pc>
     <w lemma="as" pos="acp" xml:id="A66208-001-a-1780">as</w>
     <w lemma="much" pos="d" xml:id="A66208-001-a-1790">much</w>
     <w lemma="as" pos="acp" xml:id="A66208-001-a-1800">as</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-1810">in</w>
     <w lemma="he" pos="pno" xml:id="A66208-001-a-1820">him</w>
     <w lemma="lie" pos="vvz" xml:id="A66208-001-a-1830">lies</w>
     <pc xml:id="A66208-001-a-1840">)</pc>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-1850">to</w>
     <w lemma="render" pos="vvi" xml:id="A66208-001-a-1860">Render</w>
     <w lemma="all" pos="d" xml:id="A66208-001-a-1870">all</w>
     <w lemma="just" pos="j" xml:id="A66208-001-a-1880">just</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-1890">and</w>
     <w lemma="proper" pos="j" xml:id="A66208-001-a-1900">proper</w>
     <w lemma="method" pos="n2" xml:id="A66208-001-a-1910">Methods</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-1920">of</w>
     <w lemma="prosecution" pos="n1" xml:id="A66208-001-a-1930">Prosecution</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-1940">in</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-1950">the</w>
     <w lemma="premise" pos="n2" reg="Premises" xml:id="A66208-001-a-1960">Premisses</w>
     <w lemma="ineffectual" pos="j" xml:id="A66208-001-a-1970">ineffectual</w>
     <pc xml:id="A66208-001-a-1980">,</pc>
     <w lemma="he" pos="pns" xml:id="A66208-001-a-1990">he</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-2000">the</w>
     <w lemma="say" pos="vvd" xml:id="A66208-001-a-2010">said</w>
     <hi xml:id="A66208-e170">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-2020">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-2030">Mackenzie</w>
     </hi>
     <w lemma="have" pos="vvz" xml:id="A66208-001-a-2040">hath</w>
     <w lemma="withdraw" pos="vvn" xml:id="A66208-001-a-2050">withdrawn</w>
     <w lemma="himself" pos="pr" xml:id="A66208-001-a-2060">himself</w>
     <w lemma="from" pos="acp" xml:id="A66208-001-a-2070">from</w>
     <w lemma="his" pos="po" xml:id="A66208-001-a-2080">his</w>
     <w lemma="usual" pos="j" xml:id="A66208-001-a-2090">usual</w>
     <w lemma="place" pos="n1" xml:id="A66208-001-a-2100">Place</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-2110">of</w>
     <w lemma="abide" pos="vvd" xml:id="A66208-001-a-2120">Abode</w>
     <pc xml:id="A66208-001-a-2130">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-2140">and</w>
     <w lemma="abscond" pos="vvn" xml:id="A66208-001-a-2150">Absconded</w>
     <pc xml:id="A66208-001-a-2160">,</pc>
     <w lemma="intend" pos="vvg" xml:id="A66208-001-a-2170">intending</w>
     <pc xml:id="A66208-001-a-2180">,</pc>
     <w lemma="as" pos="acp" xml:id="A66208-001-a-2190">as</w>
     <w lemma="may" pos="vmb" xml:id="A66208-001-a-2200">may</w>
     <w lemma="reasonable" pos="av-j" xml:id="A66208-001-a-2210">reasonably</w>
     <w lemma="be" pos="vvi" xml:id="A66208-001-a-2220">be</w>
     <w lemma="suppose" pos="vvn" xml:id="A66208-001-a-2230">supposed</w>
     <pc xml:id="A66208-001-a-2240">,</pc>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-2250">to</w>
     <w lemma="make" pos="vvi" xml:id="A66208-001-a-2260">make</w>
     <w lemma="his" pos="po" xml:id="A66208-001-a-2270">his</w>
     <w lemma="escape" pos="n1" xml:id="A66208-001-a-2280">Escape</w>
     <w lemma="out" pos="av" xml:id="A66208-001-a-2290">out</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-2300">of</w>
     <w lemma="this" pos="d" xml:id="A66208-001-a-2310">this</w>
     <w lemma="kingdom" pos="n1" xml:id="A66208-001-a-2320">Kingdom</w>
     <pc unit="sentence" xml:id="A66208-001-a-2330">.</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-2340">And</w>
     <w lemma="whereas" pos="cs" xml:id="A66208-001-a-2350">whereas</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-2360">the</w>
     <w lemma="commons" pos="n2" xml:id="A66208-001-a-2370">Commons</w>
     <w lemma="in" pos="acp" xml:id="A66208-001-a-2380">in</w>
     <w lemma="parliament" pos="n1" xml:id="A66208-001-a-2390">Parliament</w>
     <w lemma="assemble" pos="vvn" xml:id="A66208-001-a-2400">Assembled</w>
     <pc xml:id="A66208-001-a-2410">,</pc>
     <w lemma="have" pos="vvb" xml:id="A66208-001-a-2420">have</w>
     <pc xml:id="A66208-001-a-2430">,</pc>
     <w lemma="by" pos="acp" xml:id="A66208-001-a-2440">by</w>
     <w lemma="their" pos="po" xml:id="A66208-001-a-2450">their</w>
     <w lemma="humble" pos="j" xml:id="A66208-001-a-2460">humble</w>
     <w lemma="address" pos="n1" xml:id="A66208-001-a-2470">Address</w>
     <pc xml:id="A66208-001-a-2480">,</pc>
     <w lemma="beseech" pos="vvd" xml:id="A66208-001-a-2490">besought</w>
     <w lemma="we" pos="pno" reg="Us" xml:id="A66208-001-a-2500">Vs</w>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-2510">to</w>
     <w lemma="issue" pos="vvi" xml:id="A66208-001-a-2520">Issue</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-2530">Our</w>
     <w lemma="royal" pos="j" xml:id="A66208-001-a-2540">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A66208-001-a-2550">Proclamation</w>
     <pc xml:id="A66208-001-a-2560">,</pc>
     <w lemma="for" pos="acp" xml:id="A66208-001-a-2570">for</w>
     <w lemma="secure" pos="vvg" xml:id="A66208-001-a-2580">Securing</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-2590">the</w>
     <w lemma="person" pos="n1" xml:id="A66208-001-a-2600">Person</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-2610">of</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-2620">the</w>
     <w lemma="say" pos="vvd" xml:id="A66208-001-a-2630">said</w>
     <hi xml:id="A66208-e180">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-2640">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-2650">Mackenzie</w>
      <pc xml:id="A66208-001-a-2660">,</pc>
     </hi>
     <w lemma="we" pos="pns" xml:id="A66208-001-a-2670">We</w>
     <w lemma="have" pos="vvb" xml:id="A66208-001-a-2680">have</w>
     <w lemma="think" pos="vvn" xml:id="A66208-001-a-2690">thought</w>
     <w lemma="fit" pos="j" xml:id="A66208-001-a-2700">fit</w>
     <pc xml:id="A66208-001-a-2710">,</pc>
     <w lemma="by" pos="acp" xml:id="A66208-001-a-2720">by</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-2730">the</w>
     <w lemma="advice" pos="n1" xml:id="A66208-001-a-2740">Advice</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-2750">of</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-2760">Our</w>
     <w lemma="privy" pos="j" xml:id="A66208-001-a-2770">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66208-001-a-2780">Council</w>
     <pc xml:id="A66208-001-a-2790">,</pc>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-2800">to</w>
     <w lemma="issue" pos="vvi" xml:id="A66208-001-a-2810">Issue</w>
     <w lemma="this" pos="d" xml:id="A66208-001-a-2820">this</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-2830">Our</w>
     <w lemma="royal" pos="j" xml:id="A66208-001-a-2840">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A66208-001-a-2850">Proclamation</w>
     <pc xml:id="A66208-001-a-2860">,</pc>
     <w lemma="hereby" pos="av" xml:id="A66208-001-a-2870">hereby</w>
     <w lemma="require" pos="vvg" xml:id="A66208-001-a-2880">Requiring</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-2890">and</w>
     <w lemma="command" pos="vvg" xml:id="A66208-001-a-2900">Commanding</w>
     <w lemma="all" pos="d" xml:id="A66208-001-a-2910">all</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-2920">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A66208-001-a-2930">Loving</w>
     <w lemma="subject" pos="n2" xml:id="A66208-001-a-2940">Subjects</w>
     <w lemma="whatsoever" pos="crq" xml:id="A66208-001-a-2950">whatsoever</w>
     <pc xml:id="A66208-001-a-2960">,</pc>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-2970">to</w>
     <w lemma="discover" pos="vvi" xml:id="A66208-001-a-2980">Discover</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-2990">and</w>
     <w lemma="apprehend" pos="vvb" xml:id="A66208-001-a-3000">Apprehend</w>
     <pc xml:id="A66208-001-a-3010">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3020">and</w>
     <w lemma="cause" pos="vvi" xml:id="A66208-001-a-3030">cause</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-3040">the</w>
     <w lemma="say" pos="vvd" xml:id="A66208-001-a-3050">said</w>
     <hi xml:id="A66208-e190">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-3060">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-3070">Mackenzie</w>
     </hi>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-3080">to</w>
     <w lemma="be" pos="vvi" xml:id="A66208-001-a-3090">be</w>
     <w lemma="discover" pos="vvn" xml:id="A66208-001-a-3100">Discovered</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3110">and</w>
     <w lemma="apprehend" pos="vvn" xml:id="A66208-001-a-3120">Apprehended</w>
     <pc xml:id="A66208-001-a-3130">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3140">and</w>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-3150">to</w>
     <w lemma="carry" pos="vvi" xml:id="A66208-001-a-3160">carry</w>
     <w lemma="he" pos="pno" xml:id="A66208-001-a-3170">him</w>
     <w lemma="before" pos="acp" xml:id="A66208-001-a-3180">before</w>
     <w lemma="some" pos="d" xml:id="A66208-001-a-3190">some</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-3200">of</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-3210">Our</w>
     <w lemma="justice" pos="n2" reg="justices" xml:id="A66208-001-a-3220">Iustices</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-3230">of</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-3240">the</w>
     <w lemma="peace" pos="n1" xml:id="A66208-001-a-3250">Peace</w>
     <pc xml:id="A66208-001-a-3260">,</pc>
     <w lemma="or" pos="cc" xml:id="A66208-001-a-3270">or</w>
     <w lemma="chief" pos="j" xml:id="A66208-001-a-3280">Chief</w>
     <w lemma="magistrate" pos="n2" xml:id="A66208-001-a-3290">Magistrates</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-3300">of</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-3310">the</w>
     <w lemma="county" pos="n1" xml:id="A66208-001-a-3320">County</w>
     <pc xml:id="A66208-001-a-3330">,</pc>
     <w lemma="town" pos="n1" xml:id="A66208-001-a-3340">Town</w>
     <pc xml:id="A66208-001-a-3350">,</pc>
     <w lemma="or" pos="cc" xml:id="A66208-001-a-3360">or</w>
     <w lemma="place" pos="vvb" xml:id="A66208-001-a-3370">Place</w>
     <w lemma="where" pos="crq" xml:id="A66208-001-a-3380">where</w>
     <w lemma="he" pos="pns" xml:id="A66208-001-a-3390">he</w>
     <w lemma="shall" pos="vmb" xml:id="A66208-001-a-3400">shall</w>
     <w lemma="be" pos="vvi" xml:id="A66208-001-a-3410">be</w>
     <w lemma="apprehend" pos="vvn" xml:id="A66208-001-a-3420">Apprehended</w>
     <pc xml:id="A66208-001-a-3430">,</pc>
     <w lemma="who" pos="crq" xml:id="A66208-001-a-3440">who</w>
     <w lemma="be" pos="vvb" xml:id="A66208-001-a-3450">are</w>
     <w lemma="respective" pos="av-j" xml:id="A66208-001-a-3460">respectively</w>
     <w lemma="require" pos="vvn" xml:id="A66208-001-a-3470">Required</w>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-3480">to</w>
     <w lemma="secure" pos="vvi" xml:id="A66208-001-a-3490">Secure</w>
     <w lemma="he" pos="pno" xml:id="A66208-001-a-3500">him</w>
     <pc xml:id="A66208-001-a-3510">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3520">and</w>
     <w lemma="thereof" pos="av" xml:id="A66208-001-a-3530">thereof</w>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-3540">to</w>
     <w lemma="give" pos="vvi" xml:id="A66208-001-a-3550">give</w>
     <w lemma="speedy" pos="j" xml:id="A66208-001-a-3560">speedy</w>
     <w lemma="notice" pos="n1" xml:id="A66208-001-a-3570">Notice</w>
     <w lemma="unto" pos="acp" xml:id="A66208-001-a-3580">unto</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-3590">Our</w>
     <w lemma="privy" pos="j" xml:id="A66208-001-a-3600">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66208-001-a-3610">Council</w>
     <pc xml:id="A66208-001-a-3620">,</pc>
     <w lemma="or" pos="cc" xml:id="A66208-001-a-3630">or</w>
     <w lemma="one" pos="crd" xml:id="A66208-001-a-3640">One</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-3650">of</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-3660">Our</w>
     <w lemma="principal" pos="j" xml:id="A66208-001-a-3670">Principal</w>
     <w lemma="secretary" pos="n2" xml:id="A66208-001-a-3680">Secretaries</w>
     <w lemma="of" pos="acp" xml:id="A66208-001-a-3690">of</w>
     <w lemma="state" pos="n1" xml:id="A66208-001-a-3700">State</w>
     <pc xml:id="A66208-001-a-3710">,</pc>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-3720">to</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-3730">the</w>
     <w lemma="end" pos="n1" xml:id="A66208-001-a-3740">end</w>
     <w lemma="he" pos="pns" xml:id="A66208-001-a-3750">he</w>
     <w lemma="may" pos="vmb" xml:id="A66208-001-a-3760">may</w>
     <w lemma="be" pos="vvi" xml:id="A66208-001-a-3770">be</w>
     <w lemma="forthcoming" pos="j" reg="forthcoming" xml:id="A66208-001-a-3780">forth-coming</w>
     <pc xml:id="A66208-001-a-3790">,</pc>
     <w lemma="to" pos="prt" xml:id="A66208-001-a-3800">to</w>
     <w lemma="be" pos="vvi" xml:id="A66208-001-a-3810">be</w>
     <w lemma="deal" pos="vvn" xml:id="A66208-001-a-3820">Dealt</w>
     <w lemma="withal" pos="av" xml:id="A66208-001-a-3830">withal</w>
     <pc xml:id="A66208-001-a-3840">,</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3850">and</w>
     <w lemma="proceed" pos="vvn" xml:id="A66208-001-a-3860">Proceeded</w>
     <w lemma="against" pos="acp" xml:id="A66208-001-a-3870">against</w>
     <w lemma="according" pos="j" xml:id="A66208-001-a-3880">according</w>
     <w lemma="to" pos="acp" xml:id="A66208-001-a-3890">to</w>
     <w lemma="Law" pos="n1" xml:id="A66208-001-a-3900">Law</w>
     <pc unit="sentence" xml:id="A66208-001-a-3901">.</pc>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3910">And</w>
     <w lemma="we" pos="pns" xml:id="A66208-001-a-3920">We</w>
     <w lemma="do" pos="vvb" xml:id="A66208-001-a-3930">do</w>
     <w lemma="hereby" pos="av" xml:id="A66208-001-a-3940">hereby</w>
     <w lemma="strict" pos="av-j" xml:id="A66208-001-a-3950">strictly</w>
     <w lemma="charge" pos="vvb" xml:id="A66208-001-a-3960">Charge</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-3970">and</w>
     <w lemma="command" pos="vvi" xml:id="A66208-001-a-3980">Command</w>
     <w lemma="all" pos="d" xml:id="A66208-001-a-3990">all</w>
     <w lemma="our" pos="po" xml:id="A66208-001-a-4000">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A66208-001-a-4010">Loving</w>
     <w lemma="subject" pos="n2" xml:id="A66208-001-a-4020">Subjects</w>
     <pc join="right" xml:id="A66208-001-a-4030">(</pc>
     <w lemma="as" pos="acp" xml:id="A66208-001-a-4040">as</w>
     <w lemma="they" pos="pns" xml:id="A66208-001-a-4050">they</w>
     <w lemma="will" pos="vmb" xml:id="A66208-001-a-4060">will</w>
     <w lemma="answer" pos="vvi" xml:id="A66208-001-a-4070">Answer</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-4080">the</w>
     <w lemma="contrary" pos="j" xml:id="A66208-001-a-4090">contrary</w>
     <w lemma="at" pos="acp" xml:id="A66208-001-a-4100">at</w>
     <w lemma="their" pos="po" xml:id="A66208-001-a-4110">their</w>
     <w lemma="peril" pos="n2" xml:id="A66208-001-a-4120">perils</w>
     <pc xml:id="A66208-001-a-4130">)</pc>
     <w lemma="that" pos="cs" xml:id="A66208-001-a-4140">that</w>
     <w lemma="they" pos="pns" xml:id="A66208-001-a-4150">they</w>
     <w lemma="do" pos="vvb" xml:id="A66208-001-a-4160">do</w>
     <w lemma="not" pos="xx" xml:id="A66208-001-a-4170">not</w>
     <w lemma="any" pos="d" xml:id="A66208-001-a-4180">any</w>
     <w lemma="way" pos="n2" xml:id="A66208-001-a-4190">ways</w>
     <w lemma="conceal" pos="vvb" xml:id="A66208-001-a-4200">Conceal</w>
     <pc xml:id="A66208-001-a-4210">,</pc>
     <w lemma="but" pos="acp" xml:id="A66208-001-a-4220">but</w>
     <w lemma="do" pos="vvb" xml:id="A66208-001-a-4230">do</w>
     <w lemma="discover" pos="vvi" xml:id="A66208-001-a-4240">Discover</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-4250">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66208-001-a-4260">said</w>
     <hi xml:id="A66208-e200">
      <w lemma="Roderick" pos="nn1" xml:id="A66208-001-a-4270">Roderick</w>
      <w lemma="mackenzie" pos="nn1" xml:id="A66208-001-a-4280">Mackenzie</w>
      <pc xml:id="A66208-001-a-4290">,</pc>
     </hi>
     <w lemma="to" pos="acp" xml:id="A66208-001-a-4300">to</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-4310">the</w>
     <w lemma="end" pos="n1" xml:id="A66208-001-a-4320">end</w>
     <w lemma="he" pos="pns" xml:id="A66208-001-a-4330">he</w>
     <w lemma="may" pos="vmb" xml:id="A66208-001-a-4340">may</w>
     <w lemma="be" pos="vvi" xml:id="A66208-001-a-4350">be</w>
     <w lemma="secure" pos="vvn" xml:id="A66208-001-a-4360">Secured</w>
     <w lemma="and" pos="cc" xml:id="A66208-001-a-4370">and</w>
     <w lemma="proceed" pos="vvn" xml:id="A66208-001-a-4380">Proceeded</w>
     <w lemma="against" pos="acp" xml:id="A66208-001-a-4390">against</w>
     <w lemma="according" pos="j" xml:id="A66208-001-a-4400">according</w>
     <w lemma="to" pos="acp" xml:id="A66208-001-a-4410">to</w>
     <w lemma="Law" pos="n1" xml:id="A66208-001-a-4420">Law</w>
     <pc unit="sentence" xml:id="A66208-001-a-4421">.</pc>
    </p>
    <closer xml:id="A66208-e210">
     <dateline xml:id="A66208-e220">
      <w lemma="give" pos="vvn" xml:id="A66208-001-a-4440">Given</w>
      <w lemma="at" pos="acp" xml:id="A66208-001-a-4450">at</w>
      <w lemma="our" pos="po" xml:id="A66208-001-a-4460">Our</w>
      <w lemma="court" pos="n1" xml:id="A66208-001-a-4470">Court</w>
      <w lemma="at" pos="acp" xml:id="A66208-001-a-4480">at</w>
      <w lemma="Kensington" pos="nn1" rend="hi" xml:id="A66208-001-a-4490">Kensington</w>
      <date xml:id="A66208-e240">
       <w lemma="the" pos="d" xml:id="A66208-001-a-4500">the</w>
       <w lemma="thirteen" pos="ord" xml:id="A66208-001-a-4510">Thirteenth</w>
       <w lemma="day" pos="n1" xml:id="A66208-001-a-4520">Day</w>
       <w lemma="of" pos="acp" xml:id="A66208-001-a-4530">of</w>
       <hi xml:id="A66208-e250">
        <w lemma="February" pos="nn1" xml:id="A66208-001-a-4540">February</w>
        <pc xml:id="A66208-001-a-4550">,</pc>
       </hi>
       <w lemma="1695/6." pos="crd" xml:id="A66208-001-a-4570">1695/6.</w>
       <pc unit="sentence" xml:id="A66208-001-a-4590"/>
       <w lemma="in" pos="acp" xml:id="A66208-001-a-4600">In</w>
       <w lemma="the" pos="d" xml:id="A66208-001-a-4610">the</w>
       <w lemma="eight" pos="ord" xml:id="A66208-001-a-4620">Eighth</w>
       <w lemma="year" pos="n1" xml:id="A66208-001-a-4630">Year</w>
       <w lemma="of" pos="acp" xml:id="A66208-001-a-4640">of</w>
       <w lemma="our" pos="po" xml:id="A66208-001-a-4650">Our</w>
       <w lemma="reign" pos="n1" xml:id="A66208-001-a-4660">Reign</w>
       <pc unit="sentence" xml:id="A66208-001-a-4670">.</pc>
      </date>
     </dateline>
    </closer>
    <closer xml:id="A66208-e260">
     <w lemma="God" pos="nn1" xml:id="A66208-001-a-4680">God</w>
     <w lemma="save" pos="vvb" xml:id="A66208-001-a-4690">save</w>
     <w lemma="the" pos="d" xml:id="A66208-001-a-4700">the</w>
     <w lemma="King" pos="n1" xml:id="A66208-001-a-4710">King</w>
     <pc unit="sentence" xml:id="A66208-001-a-4711">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A66208-e270">
   <div type="colophon" xml:id="A66208-e280">
    <p xml:id="A66208-e290">
     <hi xml:id="A66208-e300">
      <hi xml:id="A66208-e310">
       <w lemma="LONDON" pos="nn1" xml:id="A66208-001-a-4730">LONDON</w>
       <pc xml:id="A66208-001-a-4740">,</pc>
      </hi>
      <w lemma="print" pos="vvn" xml:id="A66208-001-a-4750">Printed</w>
      <w lemma="by" pos="acp" xml:id="A66208-001-a-4760">by</w>
      <hi xml:id="A66208-e320">
       <w lemma="Charles" pos="nn1" xml:id="A66208-001-a-4770">Charles</w>
       <w lemma="bill" pos="n1" xml:id="A66208-001-a-4780">Bill</w>
       <pc xml:id="A66208-001-a-4790">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A66208-001-a-4800">and</w>
      <w lemma="the" pos="d" xml:id="A66208-001-a-4810">the</w>
      <w lemma="executrix" pos="n1" xml:id="A66208-001-a-4820">Executrix</w>
      <w lemma="of" pos="acp" xml:id="A66208-001-a-4830">of</w>
      <hi xml:id="A66208-e330">
       <w lemma="Thomas" pos="nn1" xml:id="A66208-001-a-4840">Thomas</w>
       <w lemma="newcomb" pos="nn1" xml:id="A66208-001-a-4850">Newcomb</w>
       <pc xml:id="A66208-001-a-4860">,</pc>
      </hi>
      <w lemma="decease" pos="vvn" reg="deceased" xml:id="A66208-001-a-4870">deceas'd</w>
      <pc xml:id="A66208-001-a-4880">;</pc>
      <w lemma="printer" pos="n2" xml:id="A66208-001-a-4890">Printers</w>
      <w lemma="to" pos="acp" xml:id="A66208-001-a-4900">to</w>
      <w lemma="the" pos="d" xml:id="A66208-001-a-4910">the</w>
      <w lemma="king" pos="n2" xml:id="A66208-001-a-4920">Kings</w>
      <w lemma="most" pos="avs-d" xml:id="A66208-001-a-4930">most</w>
      <w lemma="excellent" pos="j" xml:id="A66208-001-a-4940">Excellent</w>
      <w lemma="majesty" pos="n1" xml:id="A66208-001-a-4950">Majesty</w>
      <pc unit="sentence" xml:id="A66208-001-a-4960">.</pc>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
