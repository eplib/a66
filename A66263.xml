<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A66263">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation by Marie R.</title>
    <author>England and Wales. Sovereign (1689-1694 : William and Mary)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A66263 of text R214659 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing W2549). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A66263</idno>
    <idno type="STC">Wing W2549</idno>
    <idno type="STC">ESTC R214659</idno>
    <idno type="EEBO-CITATION">12494364</idno>
    <idno type="OCLC">ocm 12494364</idno>
    <idno type="VID">62461</idno>
    <idno type="PROQUESTGOID">2240960600</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A66263)</note>
    <note>Transcribed from: (Early English Books Online ; image set 62461)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 951:48)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation by Marie R.</title>
      <author>England and Wales. Sovereign (1689-1694 : William and Mary)</author>
      <author>Mary II, Queen of England, 1662-1694.</author>
      <author>William III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by Charles Bill, and the executrix of Thomas Newcomb ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1692.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in Huntington Library.</note>
      <note>Broadside.</note>
      <note>Requires all subjects to apprehend several dozen people on the charges of treason.</note>
      <note>At head of title: By the King and Queen.</note>
      <note>At end of text: Given at our court at Whitehall, the ninth day of May, 1692.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Broadsides</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, for calling out heretors, and fencible men, to attend the King's host.</ep:title>
    <ep:author>Scotland. Sovereign </ep:author>
    <ep:publicationYear>1692</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>451</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-02</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-03</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-04</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-04</date>
    <label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A66263-t">
  <body xml:id="A66263-e0">
   <div type="proclamation" xml:id="A66263-e10">
    <pb facs="tcp:62461:1" rend="simple:additions" xml:id="A66263-001-a"/>
    <head xml:id="A66263-e20">
     <figure xml:id="A66263-e30">
      <p xml:id="A66263-e40">
       <w lemma="WR" pos="sy" xml:id="A66263-001-a-0010">WR</w>
      </p>
      <p xml:id="A66263-e50">
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0020">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0030">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0040">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0050">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0060">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0070">PENSE</w>
      </p>
      <p xml:id="A66263-e60">
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0080">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0090">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0100">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A66263-001-a-0110">DROIT</w>
      </p>
     </figure>
     <lb xml:id="A66263-e70"/>
     <w lemma="by" pos="acp" xml:id="A66263-001-a-0120">By</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-0130">the</w>
     <w lemma="king" pos="n1" xml:id="A66263-001-a-0140">King</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-0150">and</w>
     <w lemma="queen" pos="n1" xml:id="A66263-001-a-0160">Queen</w>
     <pc xml:id="A66263-001-a-0170">,</pc>
     <w lemma="a" pos="d" xml:id="A66263-001-a-0180">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A66263-001-a-0190">PROCLAMATION</w>
     <pc unit="sentence" xml:id="A66263-001-a-0200">.</pc>
    </head>
    <opener xml:id="A66263-e80">
     <signed xml:id="A66263-e90">
      <w lemma="Marie" pos="nn1" xml:id="A66263-001-a-0210">Marie</w>
      <w lemma="r." pos="ab" xml:id="A66263-001-a-0220">R.</w>
      <pc unit="sentence" xml:id="A66263-001-a-0230"/>
     </signed>
    </opener>
    <p xml:id="A66263-e100">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A66263-001-a-0240">WHereas</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-0250">Their</w>
     <w lemma="majesty" pos="n2" xml:id="A66263-001-a-0260">Majesties</w>
     <w lemma="have" pos="vvb" xml:id="A66263-001-a-0270">have</w>
     <w lemma="receive" pos="vvn" xml:id="A66263-001-a-0280">received</w>
     <w lemma="information" pos="n1" xml:id="A66263-001-a-0290">Information</w>
     <w lemma="that" pos="cs" xml:id="A66263-001-a-0300">that</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-0310">the</w>
     <w lemma="person" pos="n2" xml:id="A66263-001-a-0320">Persons</w>
     <w lemma="herein" pos="av" xml:id="A66263-001-a-0330">herein</w>
     <w lemma="after" pos="acp" xml:id="A66263-001-a-0340">after</w>
     <w lemma="particular" pos="av-j" xml:id="A66263-001-a-0350">particularly</w>
     <w lemma="name" pos="vvn" xml:id="A66263-001-a-0360">Named</w>
     <pc xml:id="A66263-001-a-0370">,</pc>
     <w lemma="have" pos="vvb" xml:id="A66263-001-a-0380">have</w>
     <w lemma="conspire" pos="vvn" xml:id="A66263-001-a-0390">Conspired</w>
     <w lemma="together" pos="av" xml:id="A66263-001-a-0400">together</w>
     <pc xml:id="A66263-001-a-0410">,</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-0420">and</w>
     <w lemma="with" pos="acp" xml:id="A66263-001-a-0430">with</w>
     <w lemma="divers" pos="j" xml:id="A66263-001-a-0440">divers</w>
     <w lemma="other" pos="d" xml:id="A66263-001-a-0450">other</w>
     <w lemma="disaffect" pos="vvn" xml:id="A66263-001-a-0460">Disaffected</w>
     <w lemma="person" pos="n2" xml:id="A66263-001-a-0470">Persons</w>
     <pc xml:id="A66263-001-a-0480">,</pc>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-0490">to</w>
     <w lemma="disturb" pos="vvi" xml:id="A66263-001-a-0500">Disturb</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-0510">and</w>
     <w lemma="destroy" pos="vvb" xml:id="A66263-001-a-0520">Destroy</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-0530">Their</w>
     <w lemma="government" pos="n1" xml:id="A66263-001-a-0540">Government</w>
     <pc xml:id="A66263-001-a-0550">,</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-0560">and</w>
     <w lemma="for" pos="acp" xml:id="A66263-001-a-0570">for</w>
     <w lemma="that" pos="d" xml:id="A66263-001-a-0580">that</w>
     <w lemma="purpose" pos="n1" xml:id="A66263-001-a-0590">Purpose</w>
     <w lemma="have" pos="vvb" xml:id="A66263-001-a-0600">have</w>
     <w lemma="abet" pos="vvn" xml:id="A66263-001-a-0610">Abetted</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-0620">and</w>
     <w lemma="adhere" pos="vvn" xml:id="A66263-001-a-0630">Adhered</w>
     <w lemma="to" pos="acp" xml:id="A66263-001-a-0640">to</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-0650">Their</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A66263-001-a-0660">Majesties</w>
     <w lemma="enemy" pos="n2" xml:id="A66263-001-a-0670">Enemies</w>
     <pc xml:id="A66263-001-a-0680">:</pc>
     <w lemma="for" pos="acp" xml:id="A66263-001-a-0690">For</w>
     <w lemma="which" pos="crq" xml:id="A66263-001-a-0700">which</w>
     <w lemma="cause" pos="n1" xml:id="A66263-001-a-0710">Cause</w>
     <w lemma="several" pos="j" xml:id="A66263-001-a-0720">several</w>
     <w lemma="warrant" pos="n2" xml:id="A66263-001-a-0730">Warrants</w>
     <w lemma="for" pos="acp" xml:id="A66263-001-a-0740">for</w>
     <w lemma="high" pos="j" xml:id="A66263-001-a-0750">High</w>
     <w lemma="treason" pos="n1" xml:id="A66263-001-a-0760">Treason</w>
     <w lemma="have" pos="vvb" xml:id="A66263-001-a-0770">have</w>
     <w lemma="late" pos="av-j" xml:id="A66263-001-a-0780">lately</w>
     <w lemma="be" pos="vvn" xml:id="A66263-001-a-0790">been</w>
     <w lemma="issue" pos="vvn" xml:id="A66263-001-a-0800">Issued</w>
     <w lemma="out" pos="av" xml:id="A66263-001-a-0810">out</w>
     <w lemma="against" pos="acp" xml:id="A66263-001-a-0820">against</w>
     <w lemma="they" pos="pno" xml:id="A66263-001-a-0830">them</w>
     <pc xml:id="A66263-001-a-0840">;</pc>
     <w lemma="but" pos="acp" xml:id="A66263-001-a-0850">but</w>
     <w lemma="they" pos="pns" xml:id="A66263-001-a-0860">they</w>
     <w lemma="have" pos="vvb" xml:id="A66263-001-a-0870">have</w>
     <w lemma="withdraw" pos="vvn" xml:id="A66263-001-a-0880">Withdrawn</w>
     <w lemma="themselves" pos="pr" xml:id="A66263-001-a-0890">themselves</w>
     <w lemma="from" pos="acp" xml:id="A66263-001-a-0900">from</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-0910">their</w>
     <w lemma="usual" pos="j" xml:id="A66263-001-a-0920">usual</w>
     <w lemma="place" pos="n2" xml:id="A66263-001-a-0930">Places</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-0940">of</w>
     <w lemma="abide" pos="vvd" xml:id="A66263-001-a-0950">Abode</w>
     <pc xml:id="A66263-001-a-0960">,</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-0970">and</w>
     <w lemma="be" pos="vvb" xml:id="A66263-001-a-0980">are</w>
     <w lemma="flee" pos="vvn" xml:id="A66263-001-a-0990">Fled</w>
     <w lemma="from" pos="acp" xml:id="A66263-001-a-1000">from</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A66263-001-a-1010">Iustice</w>
     <pc unit="sentence" xml:id="A66263-001-a-1020">.</pc>
     <w lemma="their" pos="po" xml:id="A66263-001-a-1030">Their</w>
     <w lemma="majesty" pos="n2" xml:id="A66263-001-a-1040">Majesties</w>
     <w lemma="have" pos="vvb" xml:id="A66263-001-a-1050">have</w>
     <w lemma="therefore" pos="av" xml:id="A66263-001-a-1060">therefore</w>
     <w lemma="think" pos="vvd" xml:id="A66263-001-a-1070">thought</w>
     <w lemma="fit" pos="j" xml:id="A66263-001-a-1080">fit</w>
     <pc join="right" xml:id="A66263-001-a-1090">(</pc>
     <w lemma="by" pos="acp" xml:id="A66263-001-a-1100">by</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-1110">the</w>
     <w lemma="advice" pos="n1" xml:id="A66263-001-a-1120">Advice</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1130">of</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-1140">Their</w>
     <w lemma="privy" pos="j" xml:id="A66263-001-a-1150">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66263-001-a-1160">Council</w>
     <pc xml:id="A66263-001-a-1170">)</pc>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-1180">to</w>
     <w lemma="issue" pos="vvi" xml:id="A66263-001-a-1190">Issue</w>
     <w lemma="this" pos="d" xml:id="A66263-001-a-1200">this</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-1210">Their</w>
     <w lemma="royal" pos="j" xml:id="A66263-001-a-1220">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A66263-001-a-1230">Proclamation</w>
     <pc xml:id="A66263-001-a-1240">,</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-1250">and</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-1260">Their</w>
     <w lemma="majesty" pos="n2" xml:id="A66263-001-a-1270">Majesties</w>
     <w lemma="do" pos="vvb" xml:id="A66263-001-a-1280">do</w>
     <w lemma="hereby" pos="av" xml:id="A66263-001-a-1290">hereby</w>
     <w lemma="command" pos="vvi" xml:id="A66263-001-a-1300">Command</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-1310">and</w>
     <w lemma="require" pos="vvi" xml:id="A66263-001-a-1320">Require</w>
     <w lemma="all" pos="d" xml:id="A66263-001-a-1330">all</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-1340">Their</w>
     <w lemma="love" pos="j-vg" xml:id="A66263-001-a-1350">Loving</w>
     <w lemma="subject" pos="n2" xml:id="A66263-001-a-1360">Subjects</w>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-1370">to</w>
     <w lemma="discover" pos="vvi" xml:id="A66263-001-a-1380">Discover</w>
     <pc xml:id="A66263-001-a-1390">,</pc>
     <w lemma="take" pos="vvb" xml:id="A66263-001-a-1400">Take</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-1410">and</w>
     <w lemma="apprehend" pos="vvb" xml:id="A66263-001-a-1420">Apprehend</w>
     <w lemma="Robert" pos="nn1" rend="hi" xml:id="A66263-001-a-1430">Robert</w>
     <w lemma="earl" pos="n1" xml:id="A66263-001-a-1440">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1450">of</w>
     <hi xml:id="A66263-e120">
      <w lemma="scarsdale" pos="nn1" xml:id="A66263-001-a-1460">Scarsdale</w>
      <pc xml:id="A66263-001-a-1470">,</pc>
      <w lemma="Edward" pos="nn1" xml:id="A66263-001-a-1480">Edward</w>
      <w lemma="Henry" pos="nn1" xml:id="A66263-001-a-1490">Henry</w>
     </hi>
     <w lemma="earl" pos="n1" xml:id="A66263-001-a-1500">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1510">of</w>
     <hi xml:id="A66263-e130">
      <w lemma="litchfield" pos="nn1" xml:id="A66263-001-a-1520">Litchfield</w>
      <pc xml:id="A66263-001-a-1530">,</pc>
      <w lemma="Edward" pos="nn1" xml:id="A66263-001-a-1540">Edward</w>
     </hi>
     <w lemma="lord" pos="n1" xml:id="A66263-001-a-1550">Lord</w>
     <hi xml:id="A66263-e140">
      <w lemma="Griffin" pos="nn1" xml:id="A66263-001-a-1560">Griffin</w>
      <pc xml:id="A66263-001-a-1570">,</pc>
      <w lemma="Charles" pos="nn1" xml:id="A66263-001-a-1580">Charles</w>
     </hi>
     <w lemma="earl" pos="n1" xml:id="A66263-001-a-1590">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1600">of</w>
     <hi xml:id="A66263-e150">
      <w lemma="newburgh" pos="nn1" xml:id="A66263-001-a-1610">Newburgh</w>
      <pc xml:id="A66263-001-a-1620">,</pc>
      <w lemma="Charles" pos="nn1" xml:id="A66263-001-a-1630">Charles</w>
     </hi>
     <w lemma="earl" pos="n1" xml:id="A66263-001-a-1640">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1650">of</w>
     <hi xml:id="A66263-e160">
      <w lemma="Middleton" pos="nn1" xml:id="A66263-001-a-1660">Middleton</w>
      <pc xml:id="A66263-001-a-1670">,</pc>
      <w lemma="Charles" pos="nn1" xml:id="A66263-001-a-1680">Charles</w>
     </hi>
     <w lemma="earl" pos="n1" xml:id="A66263-001-a-1690">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1700">of</w>
     <w lemma="dunmore" pos="nn1" rend="hi" xml:id="A66263-001-a-1710">Dunmore</w>
     <pc xml:id="A66263-001-a-1720">,</pc>
     <seg type="fill-in-blank" xml:id="A66263-e180">
      <w lemma="" pos="sy" xml:id="A66263-001-a-1730">_____</w>
     </seg>
     <w lemma="lord" pos="n1" xml:id="A66263-001-a-1740">Lord</w>
     <w lemma="forbes" pos="nn1" rend="hi" xml:id="A66263-001-a-1750">Forbes</w>
     <pc xml:id="A66263-001-a-1760">,</pc>
     <w lemma="elder" pos="js" xml:id="A66263-001-a-1770">Eldest</w>
     <w lemma="son" pos="n1" xml:id="A66263-001-a-1780">Son</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1790">of</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-1800">the</w>
     <w lemma="earl" pos="n1" xml:id="A66263-001-a-1810">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-1820">of</w>
     <hi xml:id="A66263-e200">
      <w lemma="granard" pos="nn1" xml:id="A66263-001-a-1830">Granard</w>
      <pc xml:id="A66263-001-a-1840">,</pc>
      <w lemma="James" pos="nn1" xml:id="A66263-001-a-1850">James</w>
      <w lemma="Griffin" pos="nn1" xml:id="A66263-001-a-1860">Griffin</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-1870">,</pc>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-1880">Esq</w>
     <w lemma="sir" pos="n1" xml:id="A66263-001-a-1890">Sir</w>
     <hi xml:id="A66263-e220">
      <w lemma="John" pos="nn1" xml:id="A66263-001-a-1900">John</w>
      <w lemma="fenwick" pos="nn1" xml:id="A66263-001-a-1910">Fenwick</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-1920">,</pc>
     <w lemma="sir" pos="n1" xml:id="A66263-001-a-1930">Sir</w>
     <hi xml:id="A66263-e230">
      <w lemma="Theophilus" pos="nn1" xml:id="A66263-001-a-1940">Theophilus</w>
      <w lemma="oglethorpe" pos="nn1" xml:id="A66263-001-a-1950">Oglethorpe</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-1960">,</pc>
     <w lemma="sir" pos="n1" xml:id="A66263-001-a-1970">Sir</w>
     <hi xml:id="A66263-e240">
      <w lemma="Andrew" pos="nn1" xml:id="A66263-001-a-1980">Andrew</w>
      <w lemma="Forrester" pos="nn1" xml:id="A66263-001-a-1990">Forrester</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-2000">,</pc>
     <w lemma="colonel" pos="n1" xml:id="A66263-001-a-2010">Colonel</w>
     <hi xml:id="A66263-e250">
      <w lemma="Henry" pos="nn1" xml:id="A66263-001-a-2020">Henry</w>
      <w lemma="Slingsby" pos="nn1" xml:id="A66263-001-a-2030">Slingsby</w>
      <pc xml:id="A66263-001-a-2040">,</pc>
      <w lemma="James" pos="nn1" xml:id="A66263-001-a-2050">James</w>
      <w lemma="Grahme" pos="nn1" xml:id="A66263-001-a-2060">Grahme</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-2070">,</pc>
     <w lemma="esquire" pos="n1" xml:id="A66263-001-a-2080">Esquire</w>
     <pc xml:id="A66263-001-a-2090">,</pc>
     <seg type="fill-in-blank" xml:id="A66263-e260">
      <w lemma="" pos="sy" xml:id="A66263-001-a-2100">_____</w>
     </seg>
     <w lemma="orby" pos="nn1" rend="hi" xml:id="A66263-001-a-2110">Orby</w>
     <pc xml:id="A66263-001-a-2120">,</pc>
     <w lemma="second" pos="ord" xml:id="A66263-001-a-2130">Second</w>
     <w lemma="son" pos="n1" xml:id="A66263-001-a-2140">Son</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-2150">of</w>
     <w lemma="sir" pos="n1" xml:id="A66263-001-a-2160">Sir</w>
     <hi xml:id="A66263-e280">
      <w lemma="Thomas" pos="nn1" xml:id="A66263-001-a-2170">Thomas</w>
      <w lemma="orby" pos="nn1" xml:id="A66263-001-a-2180">Orby</w>
     </hi>
     <w lemma="decease" pos="j-vn" xml:id="A66263-001-a-2190">Deceased</w>
     <pc xml:id="A66263-001-a-2200">,</pc>
     <w lemma="colonel" pos="n1" xml:id="A66263-001-a-2210">Colonel</w>
     <hi xml:id="A66263-e290">
      <w lemma="Edward" pos="nn1" xml:id="A66263-001-a-2220">Edward</w>
      <w lemma="Sackvile" pos="nn1" xml:id="A66263-001-a-2230">Sackvile</w>
      <pc xml:id="A66263-001-a-2240">,</pc>
      <w lemma="Oliver" pos="nn1" xml:id="A66263-001-a-2250">Oliver</w>
      <w lemma="st." pos="ab" xml:id="A66263-001-a-2260">St.</w>
     </hi>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-2280">Esq</w>
     <w lemma="son" pos="n1" xml:id="A66263-001-a-2290">Son</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-2300">of</w>
     <w lemma="sir" pos="n1" xml:id="A66263-001-a-2310">Sir</w>
     <hi xml:id="A66263-e310">
      <w lemma="Oliver" pos="nn1" xml:id="A66263-001-a-2320">Oliver</w>
      <w lemma="st" pos="ab" xml:id="A66263-001-a-2330">St</w>
      <w lemma="George" pos="nn1" xml:id="A66263-001-a-2340">George</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-2350">,</pc>
     <w lemma="major" pos="j" xml:id="A66263-001-a-2360">Major</w>
     <hi xml:id="A66263-e320">
      <w lemma="Thomas" pos="nn1" xml:id="A66263-001-a-2370">Thomas</w>
      <w lemma="soaper" pos="n1" xml:id="A66263-001-a-2380">Soaper</w>
      <pc xml:id="A66263-001-a-2390">,</pc>
      <w lemma="Charles" pos="nn1" xml:id="A66263-001-a-2400">Charles</w>
      <w lemma="adderley" pos="nn1" xml:id="A66263-001-a-2410">Adderley</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-2420">,</pc>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-2430">Esq</w>
     <hi xml:id="A66263-e340">
      <w lemma="David" pos="nn1" xml:id="A66263-001-a-2440">David</w>
      <w lemma="lloyd" pos="nn1" xml:id="A66263-001-a-2450">Lloyd</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-2460">,</pc>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-2470">Esq</w>
     <hi xml:id="A66263-e360">
      <w lemma="George" pos="nn1" xml:id="A66263-001-a-2480">George</w>
      <w lemma="porter" pos="n1" xml:id="A66263-001-a-2490">Porter</w>
     </hi>
     <pc rend="follows-hi" xml:id="A66263-001-a-2500">,</pc>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-2510">Esq</w>
     <w lemma="son" pos="n1" xml:id="A66263-001-a-2520">Son</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-2530">of</w>
     <hi xml:id="A66263-e380">
      <w lemma="Thomas" pos="nn1" xml:id="A66263-001-a-2540">Thomas</w>
      <w lemma="porter" pos="n1" xml:id="A66263-001-a-2550">Porter</w>
     </hi>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-2560">Esq</w>
     <w lemma="decease" pos="j-vn" xml:id="A66263-001-a-2570">Deceased</w>
     <pc xml:id="A66263-001-a-2580">,</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-2590">and</w>
     <hi xml:id="A66263-e400">
      <w lemma="Edward" pos="nn1" xml:id="A66263-001-a-2600">Edward</w>
      <w lemma="Stafford" pos="nn1" xml:id="A66263-001-a-2610">Stafford</w>
     </hi>
     <w lemma="Esq" pos="ab" rend="abbr" xml:id="A66263-001-a-2620">Esq</w>
     <w lemma="wherever" pos="crq" reg="wherever" xml:id="A66263-001-a-2630">whereever</w>
     <w lemma="may" pos="vmb" xml:id="A66263-001-a-2640">may</w>
     <w lemma="be" pos="vvi" xml:id="A66263-001-a-2650">be</w>
     <w lemma="find" pos="vvn" xml:id="A66263-001-a-2660">found</w>
     <pc xml:id="A66263-001-a-2670">,</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-2680">and</w>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-2690">to</w>
     <w lemma="carry" pos="vvi" xml:id="A66263-001-a-2700">Carry</w>
     <w lemma="they" pos="pno" xml:id="A66263-001-a-2710">them</w>
     <w lemma="before" pos="acp" xml:id="A66263-001-a-2720">before</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-2730">the</w>
     <w lemma="next" pos="ord" xml:id="A66263-001-a-2740">next</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A66263-001-a-2750">Iustice</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-2760">of</w>
     <w lemma="peace" pos="n1" xml:id="A66263-001-a-2770">Peace</w>
     <pc xml:id="A66263-001-a-2780">,</pc>
     <w lemma="or" pos="cc" xml:id="A66263-001-a-2790">or</w>
     <w lemma="chief" pos="j" xml:id="A66263-001-a-2800">Chief</w>
     <w lemma="magistrate" pos="n1" xml:id="A66263-001-a-2810">Magistrate</w>
     <pc xml:id="A66263-001-a-2820">,</pc>
     <w lemma="who" pos="crq" xml:id="A66263-001-a-2830">who</w>
     <w lemma="be" pos="vvz" xml:id="A66263-001-a-2840">is</w>
     <w lemma="hereby" pos="av" xml:id="A66263-001-a-2850">hereby</w>
     <w lemma="require" pos="vvn" xml:id="A66263-001-a-2860">Required</w>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-2870">to</w>
     <w lemma="commit" pos="vvi" xml:id="A66263-001-a-2880">Commit</w>
     <w lemma="they" pos="pno" xml:id="A66263-001-a-2890">them</w>
     <w lemma="to" pos="acp" xml:id="A66263-001-a-2900">to</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-2910">the</w>
     <w lemma="next" pos="ord" xml:id="A66263-001-a-2920">next</w>
     <w lemma="goal" pos="n1" xml:id="A66263-001-a-2930">Goal</w>
     <pc xml:id="A66263-001-a-2940">,</pc>
     <w lemma="there" pos="av" xml:id="A66263-001-a-2950">there</w>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-2960">to</w>
     <w lemma="remain" pos="vvi" xml:id="A66263-001-a-2970">Remain</w>
     <w lemma="until" pos="acp" xml:id="A66263-001-a-2980">until</w>
     <w lemma="they" pos="pns" xml:id="A66263-001-a-2990">they</w>
     <w lemma="be" pos="vvb" xml:id="A66263-001-a-3000">be</w>
     <w lemma="thence" pos="av" xml:id="A66263-001-a-3010">thence</w>
     <w lemma="deliver" pos="vvn" xml:id="A66263-001-a-3020">Delivered</w>
     <w lemma="by" pos="acp" xml:id="A66263-001-a-3030">by</w>
     <w lemma="due" pos="j" xml:id="A66263-001-a-3040">due</w>
     <w lemma="course" pos="n1" xml:id="A66263-001-a-3050">Course</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-3060">of</w>
     <w lemma="Law" pos="n1" xml:id="A66263-001-a-3070">Law</w>
     <pc unit="sentence" xml:id="A66263-001-a-3071">.</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-3080">And</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-3090">Their</w>
     <w lemma="majesty" pos="n2" xml:id="A66263-001-a-3100">Majesties</w>
     <w lemma="do" pos="vvb" xml:id="A66263-001-a-3110">do</w>
     <w lemma="hereby" pos="av" xml:id="A66263-001-a-3120">hereby</w>
     <w lemma="require" pos="vvi" xml:id="A66263-001-a-3130">Require</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-3140">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66263-001-a-3150">said</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A66263-001-a-3160">Iustice</w>
     <pc xml:id="A66263-001-a-3170">,</pc>
     <w lemma="or" pos="cc" xml:id="A66263-001-a-3180">or</w>
     <w lemma="other" pos="d" xml:id="A66263-001-a-3190">other</w>
     <w lemma="magistrate" pos="n1" xml:id="A66263-001-a-3200">Magistrate</w>
     <pc xml:id="A66263-001-a-3210">,</pc>
     <w lemma="immediate" pos="av-j" xml:id="A66263-001-a-3220">immediately</w>
     <w lemma="to" pos="prt" xml:id="A66263-001-a-3230">to</w>
     <w lemma="give" pos="vvi" xml:id="A66263-001-a-3240">give</w>
     <w lemma="notice" pos="n1" xml:id="A66263-001-a-3250">Notice</w>
     <w lemma="thereof" pos="av" xml:id="A66263-001-a-3260">thereof</w>
     <w lemma="to" pos="acp" xml:id="A66263-001-a-3270">to</w>
     <w lemma="they" pos="pno" xml:id="A66263-001-a-3280">Them</w>
     <pc xml:id="A66263-001-a-3290">,</pc>
     <w lemma="or" pos="cc" xml:id="A66263-001-a-3300">or</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-3310">Their</w>
     <w lemma="privy" pos="j" xml:id="A66263-001-a-3320">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66263-001-a-3330">Council</w>
     <pc unit="sentence" xml:id="A66263-001-a-3340">.</pc>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-3350">And</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-3360">Their</w>
     <w lemma="majesty" pos="n2" xml:id="A66263-001-a-3370">Majesties</w>
     <w lemma="do" pos="vvb" xml:id="A66263-001-a-3380">do</w>
     <w lemma="hereby" pos="av" xml:id="A66263-001-a-3390">hereby</w>
     <w lemma="publish" pos="vvb" xml:id="A66263-001-a-3400">Publish</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-3410">and</w>
     <w lemma="declare" pos="vvb" xml:id="A66263-001-a-3420">Declare</w>
     <w lemma="to" pos="acp" xml:id="A66263-001-a-3430">to</w>
     <w lemma="all" pos="d" xml:id="A66263-001-a-3440">all</w>
     <w lemma="person" pos="n2" xml:id="A66263-001-a-3450">Persons</w>
     <w lemma="that" pos="cs" xml:id="A66263-001-a-3460">that</w>
     <w lemma="shall" pos="vmb" xml:id="A66263-001-a-3470">shall</w>
     <w lemma="conceal" pos="vvb" xml:id="A66263-001-a-3480">Conceal</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-3490">the</w>
     <w lemma="person" pos="n2" xml:id="A66263-001-a-3500">Persons</w>
     <w lemma="abovenamed" pos="j" xml:id="A66263-001-a-3510">abovenamed</w>
     <pc xml:id="A66263-001-a-3520">,</pc>
     <w lemma="or" pos="cc" xml:id="A66263-001-a-3530">or</w>
     <w lemma="any" pos="d" xml:id="A66263-001-a-3540">any</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-3550">of</w>
     <w lemma="they" pos="pno" xml:id="A66263-001-a-3560">them</w>
     <pc xml:id="A66263-001-a-3570">,</pc>
     <w lemma="or" pos="cc" xml:id="A66263-001-a-3580">or</w>
     <w lemma="be" pos="vvi" xml:id="A66263-001-a-3590">be</w>
     <w lemma="aid" pos="vvg" xml:id="A66263-001-a-3600">Aiding</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-3610">and</w>
     <w lemma="assist" pos="vvg" xml:id="A66263-001-a-3620">Assisting</w>
     <w lemma="in" pos="acp" xml:id="A66263-001-a-3630">in</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-3640">the</w>
     <w lemma="conceal" pos="vvg" xml:id="A66263-001-a-3650">Concealing</w>
     <w lemma="of" pos="acp" xml:id="A66263-001-a-3660">of</w>
     <w lemma="they" pos="pno" xml:id="A66263-001-a-3670">them</w>
     <pc xml:id="A66263-001-a-3680">,</pc>
     <w lemma="or" pos="cc" xml:id="A66263-001-a-3690">or</w>
     <w lemma="further" pos="j-vg" xml:id="A66263-001-a-3700">Furthering</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-3710">their</w>
     <w lemma="escape" pos="n1" xml:id="A66263-001-a-3720">Escape</w>
     <pc xml:id="A66263-001-a-3730">,</pc>
     <w lemma="that" pos="cs" xml:id="A66263-001-a-3740">That</w>
     <w lemma="they" pos="pns" xml:id="A66263-001-a-3750">they</w>
     <w lemma="shall" pos="vmb" xml:id="A66263-001-a-3760">shall</w>
     <w lemma="be" pos="vvi" xml:id="A66263-001-a-3770">be</w>
     <w lemma="proceéd" pos="vvn" xml:id="A66263-001-a-3780">Proceéded</w>
     <w lemma="against" pos="acp" xml:id="A66263-001-a-3790">against</w>
     <w lemma="for" pos="acp" xml:id="A66263-001-a-3800">for</w>
     <w lemma="such" pos="d" xml:id="A66263-001-a-3810">such</w>
     <w lemma="their" pos="po" xml:id="A66263-001-a-3820">their</w>
     <w lemma="offence" pos="n1" xml:id="A66263-001-a-3830">Offence</w>
     <w lemma="with" pos="acp" xml:id="A66263-001-a-3840">with</w>
     <w lemma="the" pos="d" xml:id="A66263-001-a-3850">the</w>
     <w lemma="utmost" pos="j" xml:id="A66263-001-a-3860">utmost</w>
     <w lemma="severity" pos="n1" xml:id="A66263-001-a-3870">Severity</w>
     <w lemma="according" pos="j" xml:id="A66263-001-a-3880">according</w>
     <w lemma="to" pos="acp" xml:id="A66263-001-a-3890">to</w>
     <w lemma="Law" pos="n1" xml:id="A66263-001-a-3900">Law</w>
     <pc unit="sentence" xml:id="A66263-001-a-3901">.</pc>
    </p>
    <closer xml:id="A66263-e420">
     <dateline xml:id="A66263-e430">
      <w lemma="give" pos="vvn" xml:id="A66263-001-a-3920">Given</w>
      <w lemma="at" pos="acp" xml:id="A66263-001-a-3930">at</w>
      <w lemma="our" pos="po" xml:id="A66263-001-a-3940">Our</w>
      <w lemma="court" pos="n1" xml:id="A66263-001-a-3950">Court</w>
      <w lemma="at" pos="acp" xml:id="A66263-001-a-3960">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A66263-001-a-3970">Whitehall</w>
      <pc xml:id="A66263-001-a-3980">,</pc>
      <date xml:id="A66263-e450">
       <w lemma="the" pos="d" xml:id="A66263-001-a-3990">the</w>
       <w lemma="nine" pos="ord" xml:id="A66263-001-a-4000">Ninth</w>
       <w lemma="day" pos="n1" xml:id="A66263-001-a-4010">Day</w>
       <w lemma="of" pos="acp" xml:id="A66263-001-a-4020">of</w>
       <w lemma="May" pos="nn1" rend="hi" xml:id="A66263-001-a-4030">May</w>
       <pc xml:id="A66263-001-a-4040">,</pc>
       <w lemma="1692." pos="crd" xml:id="A66263-001-a-4050">1692.</w>
       <pc unit="sentence" xml:id="A66263-001-a-4060"/>
      </date>
      <w lemma="in" pos="acp" xml:id="A66263-001-a-4070">In</w>
      <w lemma="the" pos="d" xml:id="A66263-001-a-4080">the</w>
      <w lemma="four" pos="ord" xml:id="A66263-001-a-4090">Fourth</w>
      <w lemma="year" pos="n1" xml:id="A66263-001-a-4100">Year</w>
      <w lemma="of" pos="acp" xml:id="A66263-001-a-4110">of</w>
      <w lemma="our" pos="po" xml:id="A66263-001-a-4120">Our</w>
      <w lemma="reign" pos="n1" xml:id="A66263-001-a-4130">Reign</w>
      <pc unit="sentence" xml:id="A66263-001-a-4140">.</pc>
     </dateline>
    </closer>
    <closer xml:id="A66263-e470">
     <w lemma="God" pos="nn1" xml:id="A66263-001-a-4150">God</w>
     <w lemma="save" pos="acp" xml:id="A66263-001-a-4160">save</w>
     <w lemma="king" pos="n1" xml:id="A66263-001-a-4170">King</w>
     <w lemma="William" pos="nn1" xml:id="A66263-001-a-4180">William</w>
     <w lemma="and" pos="cc" xml:id="A66263-001-a-4190">and</w>
     <w lemma="queen" pos="n1" xml:id="A66263-001-a-4200">Queen</w>
     <w lemma="Mary" pos="nn1" xml:id="A66263-001-a-4210">Mary</w>
     <pc unit="sentence" xml:id="A66263-001-a-4220">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A66263-e480">
   <div type="colophon" xml:id="A66263-e490">
    <p xml:id="A66263-e500">
     <hi xml:id="A66263-e510">
      <w lemma="London" pos="nn1" rend="hi" xml:id="A66263-001-a-4230">London</w>
      <pc xml:id="A66263-001-a-4240">,</pc>
      <w lemma="print" pos="vvn" xml:id="A66263-001-a-4250">Printed</w>
      <w lemma="by" pos="acp" xml:id="A66263-001-a-4260">by</w>
      <hi xml:id="A66263-e530">
       <w lemma="Charles" pos="nn1" xml:id="A66263-001-a-4270">Charles</w>
       <w lemma="bill" pos="n1" xml:id="A66263-001-a-4280">Bill</w>
      </hi>
      <pc rend="follows-hi" xml:id="A66263-001-a-4290">,</pc>
      <w lemma="and" pos="cc" xml:id="A66263-001-a-4300">and</w>
      <w lemma="the" pos="d" xml:id="A66263-001-a-4310">the</w>
      <w lemma="executrix" pos="n1" xml:id="A66263-001-a-4320">Executrix</w>
      <w lemma="of" pos="acp" xml:id="A66263-001-a-4330">of</w>
      <hi xml:id="A66263-e540">
       <w lemma="Thomas" pos="nn1" xml:id="A66263-001-a-4340">Thomas</w>
       <w lemma="newcomb" pos="nn1" xml:id="A66263-001-a-4350">Newcomb</w>
      </hi>
      <w lemma="decease" pos="vvn" reg="deceased" xml:id="A66263-001-a-4360">deceas'd</w>
      <pc xml:id="A66263-001-a-4370">;</pc>
      <w lemma="printer" pos="n2" xml:id="A66263-001-a-4380">Printers</w>
      <w lemma="to" pos="acp" xml:id="A66263-001-a-4390">to</w>
      <w lemma="the" pos="d" xml:id="A66263-001-a-4400">the</w>
      <w lemma="king" pos="n1" xml:id="A66263-001-a-4410">King</w>
      <w lemma="and" pos="cc" xml:id="A66263-001-a-4420">and</w>
      <w lemma="queen" pos="n2" xml:id="A66263-001-a-4430">Queens</w>
      <w lemma="most" pos="avs-d" xml:id="A66263-001-a-4440">most</w>
      <w lemma="excellent" pos="j" xml:id="A66263-001-a-4450">Excellent</w>
      <w lemma="majesty" pos="n2" xml:id="A66263-001-a-4460">Majesties</w>
      <pc unit="sentence" xml:id="A66263-001-a-4470">.</pc>
      <w lemma="1692." pos="crd" xml:id="A66263-001-a-4480">1692.</w>
      <pc unit="sentence" xml:id="A66263-001-a-4490"/>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
